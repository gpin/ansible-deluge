# Ansible ZNC
Install and configure Deluge on CentOS/RHEL.

### Deluge configuration
```
deluge_user: deluge
deluge_group: deluge
deluge_port: 58846
deluge_auth_user: deluge
deluge_auth_password: deluge
```

## Dependencies
None.

## Example Playbook
    - hosts: whatever
      become: yes
      roles:
        - ansible-deluge